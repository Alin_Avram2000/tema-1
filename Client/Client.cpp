#include <iostream>
#include <string>
#include "../Generated/HelloOperation.grpc.pb.h"
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
	std::string nume;
	grpc_init();
	ClientContext context;
	auto hello_stub = HelloService::NewStub(grpc::CreateChannel("localhost:8888",
		grpc::InsecureChannelCredentials()));
	HelloRequest helloRequest;
	std::cout << "\nIntroduceti numele dumneavoastra: ";
	std::cin >> nume;
	helloRequest.set_name(nume);
	HelloReply response;
	auto status = hello_stub->SayHello(&context, helloRequest, &response);
}
