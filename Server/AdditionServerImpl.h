#pragma once
#include "../Generated/HelloOperation.grpc.pb.h"

class CAdditionServerImpl final : public HelloService::Service{
public:
	CAdditionServerImpl() {};
	::grpc::Status SayHello(::grpc::ServerContext* context, const ::HelloRequest* request, ::HelloReply* response) override;

};